#include <iostream>
#include <fstream>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <vector>
#include <string>
#include <pthread.h>
#include <math.h>
#include "rainfall_seq.hpp"
using namespace std;

int pthreadNum;
int rainTime;
float absorptionRate;
int n;
int timeStep;
bool waitForDrain = true; 

vector<vector<int>> elevation;
vector<vector<neighbor>> flowRatio;
vector<vector<float>> curDrops(n, vector<float>(n, 0));
vector<vector<float>> absorption(n, vector<float>(n, 0));

int num_tasks;
vector<vector<pthread_mutex_t>> lockes;
pthread_barrier_t barrier_update_water;
pthread_barrier_t barrier_all_done;

double calc_time(struct timespec start, struct timespec end) {
  double start_sec = (double)start.tv_sec*1000000000.0 + (double)start.tv_nsec;
  double end_sec = (double)end.tv_sec*1000000000.0 + (double)end.tv_nsec;
  if (end_sec < start_sec) {
    return 0;
  } else {
    return end_sec - start_sec;
  }
}

void first_traversal(int i, int j, vector<vector<float>>& curFlow) {
  // cout << "hiiiiiiiiiiiiiiiiiiiiiiiiiii" << endl; 
  // for (auto r : curDrops) {
  //       for (auto c : r) {
  //           cout << c << ", ";
  //       }
  //       cout << endl;
  //   } 
  // rain
  if (timeStep < rainTime) curDrops[i][j]++;
  // absorb water into the point
  absorption[i][j] += curDrops[i][j]>absorptionRate ? absorptionRate : curDrops[i][j];
  curDrops[i][j] -= curDrops[i][j]>absorptionRate ? absorptionRate : curDrops[i][j];
  
  // flow to neighbors
  if(curDrops[i][j]) {
    // waitForDrain = true;
    if(!flowRatio[i][j].noTrickle()) return;
    float flowingRain = curDrops[i][j] >= 1 ? 1: curDrops[i][j];
    curDrops[i][j] -= flowingRain;
    if(flowRatio[i][j].top > 0) {
      pthread_mutex_lock(&lockes[i-1][j]);
      curFlow[i-1][j] += flowingRain * flowRatio[i][j].top;
      pthread_mutex_unlock(&lockes[i-1][j]);
    } 
    if(flowRatio[i][j].down > 0) {
      pthread_mutex_lock(&lockes[i+1][j]);
      curFlow[i+1][j] += flowingRain * flowRatio[i][j].down;
      pthread_mutex_unlock(&lockes[i+1][j]);
    }

    if(flowRatio[i][j].left > 0) {
      pthread_mutex_lock(&lockes[i][j-1]);
      curFlow[i][j-1] += flowingRain * flowRatio[i][j].left;    
      pthread_mutex_unlock(&lockes[i][j-1]);
    }
    if(flowRatio[i][j].right > 0) {
      pthread_mutex_lock(&lockes[i][j+1]);
      curFlow[i][j+1] += flowingRain * flowRatio[i][j].right;
      pthread_mutex_unlock(&lockes[i][j+1]);
    }
  }
}

void* run_part_simulation(void* args) {
  int thrdID = *((int*)args);
  cout << "thrdID: " << thrdID << endl;
    while(waitForDrain) {
      vector<vector<float>> curFlow(n, vector<float>(n, 0)); 
      // first traversal: rain + absorb + flow to neighbors
      
      for (int i = thrdID * num_tasks; i < min((thrdID + 1)* num_tasks, n); ++i){
            for (int j = 0; j < n; ++j) {               
                first_traversal(i, j, curFlow);
		// cout << i << j << endl;
            }
      }
      
      pthread_barrier_wait(&barrier_all_done);
      // cout << "finish first traversal" << endl;
      waitForDrain = false;
      pthread_barrier_wait(&barrier_update_water);
      // second traversal: update 
      for (int i = thrdID * num_tasks; i < min((thrdID + 1)* num_tasks, n); ++i){
            for (int j = 0; j < n; ++j) {             
                curDrops[i][j] += curFlow[i][j];
		if (curDrops[i][j] > 0) waitForDrain = true;
            }
      }
      timeStep++;
      pthread_barrier_wait(&barrier_all_done);
      // cout << "finsh second traversal" << endl;
      // cout << "waitForDrain: " << waitForDrain << endl;
      // if(timeStep < rainTime) rainDrop(curDrops, n);
      // absorb water into the point
      // absorb(absorption, curDrops, n, absorptionRate);
      // flow to neighbors
      // waitForDrain = flow(flowRatio, curDrops, n);
      // timeStep++;
      // cout << timeStep << endl;
    }
    return NULL;
}


int main(int argc, char *argv[])
{
    if (argc != 6) {
        std::cout << "Rainfall program shoule take 5 command line argument!" << std::endl;
        return -1;
    }
    struct timespec start_time, end_time;
    pthreadNum = atoi(argv[1]);
    rainTime = atoi(argv[2]);
    absorptionRate = stof(argv[3]);
    n = atoi(argv[4]);
    string elevation_file = argv[5];
    timeStep = 0;
    elevation = readFile(elevation_file);
    
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    flowRatio = getFlowRatio(elevation, n);
    // for (auto r : flowRatio) {
    //     for (auto c : r) {
    //         cout << c.top << "|" << c.down << "|" << c.left << "|" << c.right << ", ";
    //     }
    //     cout << endl;
    // }
    curDrops = vector<vector<float>>(n, vector<float>(n, 0));
    absorption = vector<vector<float>>(n, vector<float>(n, 0));

    num_tasks = ceil(float(n) / pthreadNum);
    // init mutexes
    for (size_t i = 0; i < n; i++) {
        vector<pthread_mutex_t> line;
        for (size_t j = 0; j < n; j++) {
            line.push_back(PTHREAD_MUTEX_INITIALIZER);
        }
        lockes.push_back(line);
    }

     // init barrier
    pthread_barrier_init(&barrier_update_water, NULL, pthreadNum);
    pthread_barrier_init(&barrier_all_done, NULL, pthreadNum);
    // Create the threads
    pthread_t* threads;
    threads = (pthread_t *) malloc(pthreadNum * sizeof(pthread_t));
    for (int i = 0; i < pthreadNum; i++) {
        // pass in i as the threadID
        int* thrdID = (int *) malloc(sizeof(int));
        *thrdID = i;
        pthread_create(&threads[i], NULL, run_part_simulation, (void *)thrdID);
    }

    for (int i = 0; i < pthreadNum; i++) {
        pthread_join(threads[i],NULL);
    }

    clock_gettime(CLOCK_MONOTONIC, &end_time);
    
    // while(waitForDrain) {
    //   if(timeStep < rainTime) rainDrop(curDrops, n);
    //   // absorb water into the point
    //   absorb(absorption, curDrops, n, absorptionRate);
    //   // flow to neighbors
    //   waitForDrain = flow(flowRatio, curDrops, n);
    //   timeStep++;
    //   // cout << timeStep << endl;
    // }

    cout << "Rainfall simulation completed in "<< timeStep <<" time steps." << endl;
    double elapsed_us = calc_time(start_time, end_time);
    double elapsed_ms = elapsed_us / 1000.0;
    std::cout << "Runtime = " << elapsed_ms / 1000.0 << " seconds" << std::endl;
    for (auto r : absorption) {
        for (auto c : r) {
            cout << c << ", ";
        }
        cout << endl;
    }
    return 0;
}
