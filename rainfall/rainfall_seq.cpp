#include <iostream>
#include <fstream>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <vector>
#include <string>
#include "rainfall_seq.hpp"
using namespace std;

double calc_time(struct timespec start, struct timespec end) {
  double start_sec = (double)start.tv_sec*1000000000.0 + (double)start.tv_nsec;
  double end_sec = (double)end.tv_sec*1000000000.0 + (double)end.tv_nsec;
  if (end_sec < start_sec) {
    return 0;
  } else {
    return end_sec - start_sec;
  }
}

int main(int argc, char *argv[])
{
    if (argc != 6) {
        std::cout << "Rainfall program shoule take 5 command line argument!" << std::endl;
        return -1;
    }
    struct timespec start_time, end_time;
    int pthreadNum = atoi(argv[1]);
    int rainTime = atoi(argv[2]);
    float absorptionRate = stof(argv[3]);
    int n = atoi(argv[4]);
    string elevation_file = argv[5];
    int timeStep = 0;
    vector<vector<int>> elevation = readFile(elevation_file);
    
    clock_gettime(CLOCK_MONOTONIC, &start_time);
    vector<vector<neighbor>> flowRatio = getFlowRatio(elevation, n);
    // for (auto r : flowRatio) {
    //     for (auto c : r) {
    //         cout << c.top << "|" << c.down << "|" << c.left << "|" << c.right << ", ";
    //     }
    //     cout << endl;
    // }
    vector<vector<float>> curDrops(n, vector<float>(n, 0));
    vector<vector<float>> absorption(n, vector<float>(n, 0));
    
    bool waitForDrain = true; 
    while(waitForDrain) {
      if(timeStep < rainTime) rainDrop(curDrops, n);
      // absorb water into the point
      absorb(absorption, curDrops, n, absorptionRate);
      // flow to neighbors
      waitForDrain = flow(flowRatio, curDrops, n);
      timeStep++;
      // cout << timeStep << endl;
    }

    cout << "Rainfall simulation completed in "<< timeStep <<" time steps." << endl;
    clock_gettime(CLOCK_MONOTONIC, &end_time);
    double elapsed_us = calc_time(start_time, end_time);
    double elapsed_ms = elapsed_us / 1000.0;
    std::cout << "Runtime = " << elapsed_ms / 1000.0 << " seconds" << std::endl;
    for (auto r : absorption) {
        for (auto c : r) {
            cout << c << ", ";
        }
        cout << endl;
    }
    return 0;
}
