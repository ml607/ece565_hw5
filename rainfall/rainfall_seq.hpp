#include <iostream>
#include <fstream>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <vector>
#include <string>
#include <climits>
using namespace std;

class neighbor{
public:
    float top;
    float down;
    float left;
    float right;

    neighbor() {
        top = 0;
        down = 0;
        left = 0;
        right = 0;
    }

    neighbor(int t, int d, int l, int r, int minFlow) {
        top = t == minFlow ? 1 : 0;
        down = d == minFlow ? 1 : 0;
        left = l == minFlow ? 1 : 0;
        right = r == minFlow ? 1 : 0;
        float sum = top + down + left + right;
        if (sum) {
            top /= sum;
            down /= sum;
            left /= sum;
            right /= sum;
        }
        
    }

    neighbor(const neighbor & rhs){
        this->top = rhs.top;
        this->down = rhs.down;
        this->left = rhs.left;
        this->right = rhs.right;
    }

    neighbor& operator=(const neighbor & rhs) {
    // Guard self assignment
        if (this == &rhs)   return *this;
        this->top = rhs.top;
        this->down = rhs.down;
        this->left = rhs.left;
        this->right = rhs.right;
        return *this;
    }

    ~neighbor() {}

    int noTrickle() {
        return top + down + left + right;
    }
};

vector<vector<int>> readFile(string elevation_file) {
    ifstream file;
    vector<vector<int>> ans;
    file.open(elevation_file, ios_base::in);

    if (!file.is_open())
    {
        cout << "failed to read file";
    }

    string s;
    while (getline(file, s))
    {
        int num;
        stringstream ss(s);
        vector<int> temp;
        while(ss >> num) {
            temp.push_back(num);
        }
        ans.push_back(temp);
    }
    file.close();
    return ans;
}

neighbor calFlowRatio(int n, int i, int j, vector<vector<int>>& elevation) {
    float left = j-1 >= 0 ? elevation[i][j-1] : INT_MAX; 
    float right = j+1 < n ? elevation[i][j+1] : INT_MAX;
    float top = i-1 >= 0 ? elevation[i-1][j] : INT_MAX;
    float down = i+1 < n ? elevation[i+1][j] : INT_MAX;
    int minFlow = min(top, min(down, min(left, right)));
    // If a point has an equal or lesser elevation than any of its 4 neighbors, then no water will trickle off of that point.
    if(elevation[i][j] <= minFlow) return neighbor();
    return neighbor(top, down, left, right, minFlow);
}

vector<vector<neighbor>> getFlowRatio(vector<vector<int>>elevation, int n) {
    vector<vector<neighbor>> ans(n, vector<neighbor>(n, neighbor()));
    for(int i = 0; i < n; ++i) {
        for(int j = 0; j < n; ++j) {
            ans[i][j] = calFlowRatio(n, i, j, elevation);
        }
    }
    return ans;
}

void rainDrop(vector<vector<float>>& curDrops, int n) {
    for(int i = 0; i < n; ++i) {
        for(int j = 0; j < n; ++j) {
            curDrops[i][j]++;
        }
    }
}

void absorb(vector<vector<float>>& absorption, vector<vector<float>>& curDrops, int n, float absorptionRate) {
    for(int i = 0; i < n; ++i) {
        for(int j = 0; j < n; ++j) {
            absorption[i][j] += curDrops[i][j]>absorptionRate ? absorptionRate : curDrops[i][j];
            curDrops[i][j] -= curDrops[i][j]>absorptionRate ? absorptionRate : curDrops[i][j];
        }
    }
}

bool flow(vector<vector<neighbor>>& flowRatio, vector<vector<float>>& curDrops, int n) {
    // Calculate the number of raindrops that will next trickle to the lowest neighbor(s)
    vector<vector<float>> curFlow(n, vector<float>(n, 0));
    bool waitForDrain = false;
    for(int i = 0; i < n; ++i) {
        for(int j = 0; j < n; ++j) {
            if(curDrops[i][j]) {
                waitForDrain = true;
                if(!flowRatio[i][j].noTrickle()) continue;
                float flowingRain = curDrops[i][j] >= 1 ? 1: curDrops[i][j];
                curDrops[i][j] -= flowingRain;
                if(flowRatio[i][j].top > 0) curFlow[i-1][j] += flowingRain * flowRatio[i][j].top;
                if(flowRatio[i][j].down > 0) curFlow[i+1][j] += flowingRain * flowRatio[i][j].down;
                if(flowRatio[i][j].left > 0) curFlow[i][j-1] += flowingRain * flowRatio[i][j].left;
                if(flowRatio[i][j].right > 0) curFlow[i][j+1] += flowingRain * flowRatio[i][j].right;
            }
            
        }
    }
    // update the number of raindrops
    for(int i = 0; i < n; ++i) {
        for(int j = 0; j < n; ++j) {
            curDrops[i][j] += curFlow[i][j];
        }
    }
    return waitForDrain;

}